﻿namespace Projet_code
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnl_Choix = new System.Windows.Forms.Panel();
            this.lbl_InfoCommercial = new System.Windows.Forms.Label();
            this.btn_Valider = new System.Windows.Forms.Button();
            this.cbx_Commercial = new System.Windows.Forms.ComboBox();
            this.pnl_Facture = new System.Windows.Forms.Panel();
            this.pnl_Produit = new System.Windows.Forms.Panel();
            this.pnl_Choix.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl_Choix
            // 
            this.pnl_Choix.Controls.Add(this.lbl_InfoCommercial);
            this.pnl_Choix.Controls.Add(this.btn_Valider);
            this.pnl_Choix.Controls.Add(this.cbx_Commercial);
            this.pnl_Choix.Location = new System.Drawing.Point(58, 42);
            this.pnl_Choix.Name = "pnl_Choix";
            this.pnl_Choix.Size = new System.Drawing.Size(327, 177);
            this.pnl_Choix.TabIndex = 0;
            // 
            // lbl_InfoCommercial
            // 
            this.lbl_InfoCommercial.AutoSize = true;
            this.lbl_InfoCommercial.Location = new System.Drawing.Point(186, 111);
            this.lbl_InfoCommercial.Name = "lbl_InfoCommercial";
            this.lbl_InfoCommercial.Size = new System.Drawing.Size(82, 17);
            this.lbl_InfoCommercial.TabIndex = 2;
            this.lbl_InfoCommercial.Text = "voici du text";
            // 
            // btn_Valider
            // 
            this.btn_Valider.Location = new System.Drawing.Point(189, 26);
            this.btn_Valider.Name = "btn_Valider";
            this.btn_Valider.Size = new System.Drawing.Size(84, 24);
            this.btn_Valider.TabIndex = 1;
            this.btn_Valider.Text = "Valider";
            this.btn_Valider.UseVisualStyleBackColor = true;
            this.btn_Valider.Click += new System.EventHandler(this.btn_Valider_Click);
            // 
            // cbx_Commercial
            // 
            this.cbx_Commercial.FormattingEnabled = true;
            this.cbx_Commercial.Items.AddRange(new object[] {
            "Poulet Mayonnaise",
            "Poulet Currie",
            "Poulet Champignon"});
            this.cbx_Commercial.Location = new System.Drawing.Point(36, 26);
            this.cbx_Commercial.Name = "cbx_Commercial";
            this.cbx_Commercial.Size = new System.Drawing.Size(121, 24);
            this.cbx_Commercial.TabIndex = 0;
            // 
            // pnl_Facture
            // 
            this.pnl_Facture.Location = new System.Drawing.Point(407, 42);
            this.pnl_Facture.Name = "pnl_Facture";
            this.pnl_Facture.Size = new System.Drawing.Size(323, 177);
            this.pnl_Facture.TabIndex = 1;
            // 
            // pnl_Produit
            // 
            this.pnl_Produit.Location = new System.Drawing.Point(58, 244);
            this.pnl_Produit.Name = "pnl_Produit";
            this.pnl_Produit.Size = new System.Drawing.Size(672, 179);
            this.pnl_Produit.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pnl_Produit);
            this.Controls.Add(this.pnl_Facture);
            this.Controls.Add(this.pnl_Choix);
            this.Name = "Form1";
            this.Text = "Form1";
            this.pnl_Choix.ResumeLayout(false);
            this.pnl_Choix.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnl_Choix;
        private System.Windows.Forms.Panel pnl_Facture;
        private System.Windows.Forms.ComboBox cbx_Commercial;
        private System.Windows.Forms.Panel pnl_Produit;
        private System.Windows.Forms.Button btn_Valider;
        private System.Windows.Forms.Label lbl_InfoCommercial;
    }
}

