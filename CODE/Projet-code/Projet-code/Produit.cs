﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_code
{
    class Produit
    {
        int reference;
        double prix;
        string nom;

        public Produit(int uneReference, double unPrix, string unNom)
        {
            this.Reference = uneReference;
            this.Prix = unPrix;
            this.Nom = unNom;
        }

        public int Reference { get => reference; set => reference = value; }
        public double Prix { get => prix; set => prix = value; }
        public string Nom { get => nom; set => nom = value; }
    }
}
