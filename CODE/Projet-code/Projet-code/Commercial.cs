﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_code
{
    class Commercial
    {
        string nomCommercial;
        string prenomCommercial;
        string facture;
        int id;
        double pourcentage;
        string identifiant;
        string motDePasse;

        //public double Pourcentage { get => pourcentage; set => pourcentage = value; 

        public Commercial(int unId, string unNomCommercial, string unPrenomCommercial,  double unPourcentage, string unIdentifiant, string unMotDePasse)
        {
            this.id = unId;
            this.nomCommercial = unNomCommercial;
            this.prenomCommercial = unPrenomCommercial;
            this.pourcentage = unPourcentage;
            this.identifiant = unIdentifiant;
            this.motDePasse = unMotDePasse;
        }
        public int getId()
        {
            return this.id;
        }
        public void setId(int unId)
        {
            id = unId;
        }
        public string getNomCommercial()
        {
            return this.nomCommercial;
        }
        public void setNomCommercial(string unNomCommercial)
        {
            this.nomCommercial = unNomCommercial;
        }
        public string getPrenomCommercial()
        {
            return this.prenomCommercial;
        }
        public void setPrenomCommercial(string unPrenomCommercial)
        {
            prenomCommercial = unPrenomCommercial;
        }
        public double getPourcentage()
        {
            return this.pourcentage;
        }
        public void setPourcentage(double unPourcentage)
        {
            pourcentage = unPourcentage;
        }
        public string getIdentifiant()
        {
            return this.identifiant;
        } 
        public void setIdentifiant(string unIdentifiant)
        {
            identifiant = unIdentifiant;
        }
        public string motdepasse()
        {
            return this.motDePasse;
        }
        public void setMotDePasse(string unMotDePasse)
        {
            motDePasse = unMotDePasse;
        }

        
    }
}
