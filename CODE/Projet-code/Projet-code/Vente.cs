﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_code
{
    class Vente
    {
        int idVente;
        DateTime date;
        string nomClient;
        bool venteEffectuee;

        public Vente(int idVente, DateTime date, string nomClient, bool venteEffectuee)
        {
            this.IdVente = idVente;
            this.Date = date;
            this.NomClient = nomClient;
            this.VenteEffectuee = venteEffectuee;
        }

        public int IdVente { get => idVente; set => idVente = value; }
        public DateTime Date { get => date; set => date = value; }
        public string NomClient { get => nomClient; set => nomClient = value; }
        public bool VenteEffectuee { get => venteEffectuee; set => venteEffectuee = value; }
    }
}
