﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_code
{
    class Facture
    {
        int idFacture;
        DateTime dateFacture;

        public Facture(int idFacture, DateTime dateFacture)
        {
            this.IdFacture = idFacture;
            this.DateFacture = dateFacture;
        }

        public int IdFacture { get => idFacture; set => idFacture = value; }
        public DateTime DateFacture { get => dateFacture; set => dateFacture = value; }
    }
}
