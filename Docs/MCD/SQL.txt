CREATE TABLE IF NOT EXISTS `mydb`.`vente` (
  `idvente` INT NOT NULL,
  `datevente` DATE NULL,
  `nomclient` VARCHAR(45) NULL,
  `venteeffectue` BLOB NULL,
  PRIMARY KEY (`idvente`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`facture`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`facture` (
  `idfacture` INT NOT NULL,
  `datefacture` DATE NULL,
  `vente_idvente` INT NOT NULL,
  PRIMARY KEY (`idfacture`),
  INDEX `fk_facture_vente1_idx` (`vente_idvente` ASC),
  CONSTRAINT `fk_facture_vente1`
    FOREIGN KEY (`vente_idvente`)
    REFERENCES `mydb`.`vente` (`idvente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`commercial`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`commercial` (
  `id` INT NOT NULL,
  `nomCommercial` VARCHAR(45) NULL,
  `prenomCommercial` VARCHAR(45) NULL,
  `pourcentage` DECIMAL(10) NULL,
  `identifiant` VARCHAR(45) NULL,
  `motdepasse` VARCHAR(45) NULL,
  `vente_idvente` INT NOT NULL,
  `facture_idfacture` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_commercial_vente_idx` (`vente_idvente` ASC),
  INDEX `fk_commercial_facture1_idx` (`facture_idfacture` ASC),
  CONSTRAINT `fk_commercial_vente`
    FOREIGN KEY (`vente_idvente`)
    REFERENCES `mydb`.`vente` (`idvente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_commercial_facture1`
    FOREIGN KEY (`facture_idfacture`)
    REFERENCES `mydb`.`facture` (`idfacture`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`produit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`produit` (
  `reference` VARCHAR(45) NOT NULL,
  `prix` DECIMAL(65) NULL,
  `nom` VARCHAR(45) NULL,
  PRIMARY KEY (`reference`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`vente_has_produit`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`vente_has_produit` (
  `vente_idvente` INT NOT NULL,
  `produit_reference` VARCHAR(45) NOT NULL,
  `quantiteproduit` INT NULL,
  `prixvente` DECIMAL(65) NULL,
  PRIMARY KEY (`vente_idvente`, `produit_reference`),
  INDEX `fk_vente_has_produit_produit1_idx` (`produit_reference` ASC),
  INDEX `fk_vente_has_produit_vente1_idx` (`vente_idvente` ASC),
  CONSTRAINT `fk_vente_has_produit_vente1`
    FOREIGN KEY (`vente_idvente`)
    REFERENCES `mydb`.`vente` (`idvente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_vente_has_produit_produit1`
    FOREIGN KEY (`produit_reference`)
    REFERENCES `mydb`.`produit` (`reference`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;